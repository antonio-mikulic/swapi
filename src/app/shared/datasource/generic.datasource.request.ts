export interface IGenericDatasourceRequest {
  filter?: string;
  take?: number;
  skip?: number;
}
