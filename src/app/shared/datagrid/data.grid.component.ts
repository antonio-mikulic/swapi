import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { MatMenuPanel } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';

import { GenericDataSource } from '../datasource/generic.datasource';
import { ColumnConfig } from './ColumnConfig';

@Component({
  selector: 'data-grid',
  templateUrl: './data.grid.component.html',
  styleUrls: ['./data.grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataGridComponent implements AfterViewInit {
  @ViewChild(MatPaginator) private paginator!: MatPaginator;
  @ViewChild(MatTable) private table!: MatTable<any>;
  @ViewChild('input') private input!: ElementRef;

  @Input() columns: ColumnConfig[] = [];
  @Input() dataSource?: GenericDataSource<any>;
  @Input() actionMenu?: MatMenuPanel<any>;
  @Input() pageSize = 5;
  @Input() pageSizeOptions = [2, 5, 10];

  @Output() searchCleared: EventEmitter<any> = new EventEmitter<any>();

  displayedColumns: string[] = [];

  public get TableComponent(): MatTable<any> {
    return this.table;
  }

  public get PaginatorComponent(): MatPaginator {
    return this.paginator;
  }

  public get SearchElement(): ElementRef {
    return this.input;
  }

  refresh() {
    this.dataSource?.refresh();
  }

  ngAfterViewInit(): void {
    this._initData();
  }

  clearInput(): void {
    this.searchCleared.emit(null);
    this.input.nativeElement.value = '';
  }

  private _initData() {
    if (this.dataSource == null) {
      throw Error('DynamicTable must be provided with data source.');
    }
    if (this.columns == null) {
      throw Error('DynamicTable must be provided with column definitions.');
    }

    this.dataSource.table = this.table;
    this.dataSource.paginator = this.paginator;
    this.dataSource.search = this.input;

    this.displayedColumns = this.columns.filter((column => column.name)).map((column) => column.name!);
  }
}
