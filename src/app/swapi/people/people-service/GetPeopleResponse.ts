import { Person } from '@app/swapi/people/Person';

export interface GetPeopleResponse {
    count: number;
    next: string;
    previous?: string;
    results: Person[];
}
