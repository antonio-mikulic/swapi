import { IGenericDatasourceRequest } from '@app/shared/datasource/generic.datasource.request';

export class GetPeopleInput implements IGenericDatasourceRequest {
    filter: string = '';
    take?: number;
    skip?: number;
}
