import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PagedResult } from '@app/shared/datasource/PagedResult';
import { Person } from '@app/swapi/people/Person';
import { BehaviorSubject, map, Observable } from 'rxjs';

import { GetPeopleInput } from './GetPeopleInput';
import { GetPeopleResponse } from './GetPeopleResponse';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  people = new BehaviorSubject<Person[]>([]);
  pageSize = 10;
  swapiPeople = "https://swapi.dev/api/people";

  constructor(private http: HttpClient) { }

  // This implementation assumes pagination will always match server pagiation size
  // Or be less than server side, but not requiring two requests
  // So user page size of 20 won't work for getting people range from 40-60
  getPeople(input: GetPeopleInput): Observable<PagedResult<Person>> {
    this._validateTake(input);
    const page = this._getPage(input);
    const url = this._generateUrl(page, input.filter);

    return this._getPeopleFromSwapi(url, input);
  }

  private _getPage(input: GetPeopleInput): number {
    if (!input.skip) { return 1 }
    if (!input.take) { return 1 }
    const page = 1 + Math.floor(input.skip / input.take);
    return page;
  }

  private _validateTake(input: GetPeopleInput) {
    if (!input.take) { return }

    if (input.take > this.pageSize) {
      throw new Error('Take cannot be greater than page size');
    }

    if (this.pageSize % input.take !== 0) {
      throw new Error('Take must be a multiple of page size');
    }
  }

  private _getPeopleFromSwapi(url: string, input: GetPeopleInput): Observable<PagedResult<Person>> {
    return this.http.get<GetPeopleResponse>(url).pipe(map((response: GetPeopleResponse) => {
      let parsedItems = response.results;
      if (input.take) {
        const skip = input.skip ? input.skip : 0;
        parsedItems = parsedItems.slice(skip, skip + input.take);
      }
      return new PagedResult<Person>(parsedItems, response.count);
    }));
  }

  private _generateUrl(page: number, search: string) {
    let url = page > 1 ? this.swapiPeople : `${this.swapiPeople}?page=${page}`;
    url = !search ? url : url.includes('?') ? `${url}&search=${search}` : `${url}?search=${search}`;
    return url;
  }
}
