import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@app/shared/app-component-base';
import { ColumnConfig } from '@app/shared/datagrid/ColumnConfig';
import { DataGridComponent } from '@app/shared/datagrid/data.grid.component';

import { GetPeopleInput } from '../people-service/GetPeopleInput';
import { PeopleService } from '../people-service/people.service';
import { PeopleDataSource } from './people-data-source.component';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PeopleListComponent extends AppComponentBase implements AfterViewInit {
  @ViewChild(DataGridComponent) grid?: DataGridComponent;

  dataSource: PeopleDataSource;

  columns: ColumnConfig[] = [
    { name: 'name', displayName: 'Name' },
    { name: 'height', displayName: 'Height' },
    { name: 'mass', displayName: 'Mass' },
    { name: 'hair_color', displayName: 'Hair Color' },
    { name: 'skin_color', displayName: 'Skin Color' },
    { name: 'eye_color', displayName: 'Eye Color' },
    { name: 'birth_year', displayName: 'Born' },
    { name: 'gender', displayName: 'Gender' },
  ];

  constructor(
    injector: Injector,
    private _peopleService: PeopleService,
  ) {
    super(injector);
    this.dataSource = new PeopleDataSource(this._peopleService);
  }

  ngAfterViewInit(): void {
    this.dataSource.dataReloadSubject.subscribe(() => this.refresh());
  }

  refresh(): void {
    let input = new GetPeopleInput();
    this.dataSource.load(input);
  }
}
