import { Entity } from '@app/shared/datasource/Entity';
import { GenericDataSource } from '@app/shared/datasource/generic.datasource';
import { PagedResult } from '@app/shared/datasource/PagedResult';
import { Observable } from 'rxjs';

import { GetPeopleInput } from '../people-service/GetPeopleInput';
import { PeopleService } from '../people-service/people.service';
import { Person } from '../Person';

export class PeopleDataSource extends GenericDataSource<Person> {
  constructor(private dataService: PeopleService) {
    super();
  }

  protected getData(request: GetPeopleInput): Observable<PagedResult<Person>> {
    return this.dataService.getPeople(request);
  }

  protected override compareIds(a: Person, b: Person) {
    return a.name === b.name;
  }
}