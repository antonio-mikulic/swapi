import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { SharedModule } from '@app/shared/shared.module';

import { PeopleListComponent } from './people/people-list/people-list.component';
import { PeopleService } from './people/people-service/people.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    MatCardModule,
    MatButtonModule
  ],
  declarations: [
    PeopleListComponent
  ],
  exports: [
    PeopleListComponent
  ],
  providers: [
    PeopleService
  ]
})
export class SwapiModule { }
